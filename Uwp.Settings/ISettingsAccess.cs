﻿namespace Uwp.Settings
{
    internal interface ISettingsAccess
    {
        object Read<T>(string settingName);
        void Write(string settingName, object settingValue);
    }
}
