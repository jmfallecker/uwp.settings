﻿using System;
using System.Diagnostics;

// This example code shows how you could implement the required main function for a 
// Console UWP Application. You can replace all the code inside Main with your own custom code.

// You should also change the Alias value in the AppExecutionAlias Extension in the 
// Package.appxmanifest to a value that you define. To edit this file manually, right-click
// it in Solution Explorer and select View Code, or open it with the XML Editor.

namespace Uwp.Settings.Harness
{
    class Program
    {
        enum Turn { left, right }

        public class User
        {
            public string Name { get; set; }
            public override string ToString() => Name;
        }

        static void Main(string[] args)
        {
            Settings.SetDefaultDataStore(DataStore.Local);
            Settings.Clear(DataStore.Local);

            Test("Jerry");
            Test(123);
            Test(123d);
            Test(123f);
            Test(true);
            Test(DateTime.Now);
            Test(DateTimeOffset.Now);
            Test(TimeSpan.FromMinutes(1));
            Test(Turn.left);
            Test(new User { Name = "Jerry" });

            Console.ReadLine();
        }

        private static void Test<T>(T value)
        {
            var key = typeof(T).ToString();

            Console.WriteLine(string.Empty);
            Console.WriteLine($"Testing {key}:{value}");

            Console.ForegroundColor = ConsoleColor.Red;

            try
            {
                var existing = Settings.Read<T>(key);
                if (Equals(existing, default(T)))
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                Console.WriteLine($"Pre-reading: Equals({existing}, {default(T)}) = {Equals(existing, default(T))}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error pre-reading: {ex.Message}");
            }
            finally
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }

            try
            {
                Settings.Write(key, value);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error writing: {ex.Message}");
            }

            try
            {
                var storage = Settings.Read<T>(key);
                if (Equals(storage, value))
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                Console.WriteLine($"Post-reading: Equals({storage}, {value}) = {Equals(storage, value)}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error post-reading: {ex.Message}");
            }
            finally
            {
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
